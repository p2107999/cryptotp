import math
import random

from sympy import *
from random import *


def getprime(k):
    p = randprime(2 ** (k - 1), 2 ** k)

    return p


def genkeys(k):
    p = getprime(k)

    q = getprime(k)

    while (p == q):
        q = getprime(k)

    N = int(p * q)

    Phi = int(N - p - q + 1)

    return [N, mod_inverse(N, Phi)]


def encrypt(m, pk):
    # Choisir r aléatoirement dans {0,...,pk-1}
    r = randint(0, pk - 1)

    N2 = pk * pk

    c = ((1 + m * pk) * pow(r, pk, N2)) % N2

    return int(c)


def decrypt(c, pk, sk):
    N2 = pk * pk

    r = pow((c % pk), sk, pk)

    s = pow(r, -pk, N2)

    m = ((c * s) % N2 - 1) // pk

    return int(m)


# Propriteté homomoprhe
def oplus(X, Y, pk):
    Z = (X * Y) % (pk ** 2)
    return Z


def produitParConstante(X, y, pk):
    Z = pow(X, y, pk ** 2)
    return Z


def oppose(X, pk):
    Z = pow(X, -1, pk ** 2)
    return Z


# ------------------Code  TP NOTE- Maris,Dupré,Auer --------------------------------------------------

pk, sk = genkeys(300)
print('===========Alice=============')
ixa = 0
print('X Choosen : ', ixa)

iya = 0
print('Y Choosen : ', iya)
print('==============================')

#--------------------
# Choix Position Alice
#-----------------------
xa = ixa
ya = iya
Xa = encrypt(xa, pk)
Ya = encrypt(ya, pk)

#
# Envoi Simulé par le partage des variable encrypté Xa et Ya

print('============Bob==============')
ixb = 80
print('X Choosen : ', ixb)
iyb = 10
print('Y Choosen : ', iyb)
print('==============================')

# Choix Position Bob
xb = ixb
yb = iyb
Xb = encrypt(xb, pk)
Yb = encrypt(yb, pk)


def DistanceBob(xa, xb, ya, yb, pk, sk):
    # alice envoie [xa] et [ya] (encrypté)
    Xa = encrypt(xa, pk)
    Ya = encrypt(ya, pk)
    mess_alice_xa = Xa
    mess_alice_ya = Ya

    # bob envoie [x²b + y²b - 2(xa  * xb + ya * yb)
    membre_gauche = encrypt((xb * xb + yb * yb), pk)
    Xaxb = produitParConstante(mess_alice_xa, -2 * xb, pk)
    Yayb = produitParConstante(mess_alice_ya, -2 * yb, pk)
    Xaxb_Yayb = oplus(Xaxb, Yayb, pk)
    # Bob envoie son message
    message_bob = oplus(membre_gauche, Xaxb_Yayb, pk)

    # alice decrypte le message
    distance_ab_cr = oplus(message_bob, encrypt(xa ** 2 + ya**2, pk), pk)
    distance_AB = pow(decrypt(distance_ab_cr, pk, sk), 0.5)

    print(f"Distance Alice-Bob {distance_AB}")
    return distance_AB


# Question 2 -> savoir si bob est dans la zone ou non
def DistanceBob100(xa, xb, ya, yb, pk, sk):
    # alice envoie [xa] et [ya] (encrypté)
    Xa = encrypt(xa, pk)
    Ya = encrypt(ya, pk)
    Xa2_Ya2 = encrypt((xa ** 2 + ya ** 2), pk)
    mess_alice_xa = Xa
    mess_alice_ya = Ya

    # bob envoi un tableau de valeurs possible

    membre_gauche = encrypt((xb * xb + yb * yb), pk)
    Xaxb = produitParConstante(mess_alice_xa, -2 * xb, pk)
    Yayb = produitParConstante(mess_alice_ya, -2 * yb, pk)
    Xaxb_Yayb = oplus(Xaxb, Yayb, pk)

    # Bob envoie son message
    m_gauche_Xaxb_Yayb = oplus(membre_gauche, Xaxb_Yayb, pk)
    message_bob = oplus(m_gauche_Xaxb_Yayb, Xa2_Ya2, pk)
    tab = []
    for i in range(0,10001):
        random_n = randint(0, pk-1)
        new_mess = produitParConstante(encrypt(i, pk), -1, pk)
        new_mess = oplus(message_bob, new_mess, pk)
        new_mess = produitParConstante(new_mess, random_n, pk)
        tab.append(new_mess)
        if i % 100 == 0:
            print(i)
    shuffle(tab)


    # Alice cherche un zero dans les données
    for index, i in enumerate(tab):
        dec = pow(decrypt(i, pk, sk), 0.5)
        if index % 100 == 0:
            print(f"Decrypt: {index}")
        if dec == 0:
            print("Alerte, Bob est mAlICiEux et est dans une zone proche de vous")
            return True
    return False

def LocalisationBob100(xa, xb, ya, yb, pk, sk):
    # Alice envoie [xA], [x2A] et Xa2_Ya2
    Xa = encrypt(xa, pk)
    Xb = encrypt(xb, pk)
    Xa2_Ya2 = encrypt((xa ** 2 + ya ** 2), pk)

    mess_alice_xa = Xa
    mess_alice_ya = Ya

    # bob envoi un tableau de valeurs possible
    membre_gauche = encrypt((xb * xb + yb * yb), pk)
    Xaxb = produitParConstante(mess_alice_xa, -2 * xb, pk)
    Yayb = produitParConstante(mess_alice_ya, -2 * yb, pk)
    Xaxb_Yayb = oplus(Xaxb, Yayb, pk)
    # Bob envoie son message
    m_gauche_Xaxb_Yayb = oplus(membre_gauche, Xaxb_Yayb, pk)
    message_bob = oplus(m_gauche_Xaxb_Yayb, Xa2_Ya2, pk)
    tab = []
    for i in range(0, 10001) :
        tabTmp = []
        random_n = randint(0, pk - 1)
        new_mess = produitParConstante(encrypt(i, pk), -1, pk)
        temp = oplus(message_bob, new_mess, pk)
        new_mess = produitParConstante(temp, random_n, pk)
        tabTmp.append(new_mess)
        #XB
        random_2 = randint(0, pk - 1)
        tabTmp.append(oplus(Xb,produitParConstante(temp,random_2,pk),pk))
        #YB
        random_3 = randint(0, pk - 1)
        tabTmp.append(oplus(Yb, produitParConstante(temp, random_3, pk), pk))

        tab.append(tabTmp)

        if i % 100 == 0:
            print(i)
    shuffle(tab)

    # Alice cherche un zero dans les données
    for index, i in enumerate(tab):
        dec = decrypt(i[0], pk, sk)
        if dec == 0 :
            return [decrypt(i[1], pk, sk), decrypt(i[2],pk,sk)]
    return False



#### TEST DES METHODES ###


DistanceBob(xa, xb, ya, yb, pk, sk)
#print(DistanceBob100(xa, xb, ya, yb, pk, sk))
#print(LocalisationBob100(xa, xb, ya, yb, pk, sk))
