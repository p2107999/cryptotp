# Q1 

Dans ce projet, afin d'encrypter les données, nous allons utiliser l'encryption de Pailllet, principalement pour ses propriétés homomorphiques.

Dans ce protocole, le distance dAB = p(xB − xA)2 + (yB − yA)2 va être décomposer en deux sous calcul qui vont permettre d'être fait de manière plus sécurisés

``
[xb^2 + yb^2 -2(xaxb+ yayb)] + [xa^2+ya^2]
``


détail du protocol DistanceBob : 

    Alice chiffre ses coordonnées (xa, ya) avec la clé publique pk de Paillier.
    Elle obtient ainsi deux valeurs chiffrées Xa et Ya.

    Bob calcule xb² + yb², les carrés de ses propres coordonnées, puis chiffre le résultat avec la clé publique pk.
    Il obtient ainsi membre_gauche, qui représente xb² + yb² chiffré.

    Bob calcule -2(xa * xb + ya * yb) en multipliant les coordonnées chiffrées d'Alice par les coordonnées de Bob et en multipliant le résultat par -2.
    Il obtient ainsi Xaxb_Yayb, qui représente le résultat de la partie droite de la formule de distance chiffré.

    Il obtient ainsi Xaxb_Yayb, qui représente le résultat de la partie droite de la formule de distance chiffré

    Bob calcule le résultat final en additionnant membre_gauche et Xaxb_Yayb (les parties gauche et droite de la formule de distance chiffrées)

    Alice ajoute les éléments manquants que elle seul connait pour calculer la distance distance_ab_cr = oplus(message_bob, encrypt(xa ** 2 + ya**2, pk), pk)

    Ensuite elle retourne la distance.

# Q2

_Voir fonction DistanceBob_

# Q3

Ce protocole n'est pas vraiment sûr pour Bob, en effet, Alice sait à quelle distance se trouve Bob lorsqu'elle décrypte le message de bob.
Il faudrait renforcer la sécurité du protocole pour bob afin qu'Alice ne puisse pas savoir où se trouve bob de manière précise.
D'un autre côté, Alice ne craint rien de Bob, ce dernier n'a pas accès à sa position. En effet, il ne peut pas accéder aux données de son bracelet.
Même s'il le pouvait, Alice transmet des données chiffrées que Bob ne peut pas déchiffrer.

# Q4

Bob doit renvoyer un tableau de coordonnée d'une taille 10000.
Il doit modifier les calculs qu'il effectue pour générer un tableau de telle sorte que chaque ligne soit comme suit :

![img.png](img.png)


Comme on souhaite savoir s'il est à moins de 100m d'Alice, il va générer une valeur pour les 10000 valeurs possibles (100²).

Attention avec l'information de l'indice du tableau Alice pourrait retrouver la position de Bob et pour eviter cela nous allons shuffle(tab) notre tableau.

Quand Alice va decrypter chaque ligne du tableau, elle trouvera soit uniquement des valeurs aléatoires, soit des valeurs aléatoires et 1 zéro.
Si elle trouve un zéro, elle saura que Bob est à moins de 100m d'elle.

# Q5

Ce protocole est bien plus sûr pour Bob que le précédent. En effet, Alice ne peut pas savoir où se trouve Bob de manière précise.
Elle sait juste si Bob se trouve dans un périmètre de 100m autour d'elle.
Pour Alice, la sécurité ne change pas puisqu'elle transmet toujours des données chiffrées à Bob. Données auxquelles Bob ne peut pas accéder.

# Q6

_Voir fonction DistanceBob100_

# Q7

_Voir fonction LocalisationBob100_
